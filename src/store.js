import { createStore, applyMiddleware } from 'redux';

const reducer = (state = [], action) => {
    if (action.type === 'ADD_ACTIVIDADES') {
        
        return {
            actividades : action.actividades
        };
    }
}

const logger = store => next => action => {
    console.log('dispatching', action)
    let result = next(action)
    console.log('next state', store.getState())
    return result
}

export default createStore(reducer, [], applyMiddleware(logger));