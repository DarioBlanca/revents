import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { App } from './components/App/App';
import { Login } from './components/Login/Login';
import { Registration } from './components/Registration/Registration';

export const AppRouter = () => 
     <Switch>
        <Route path="/login" component={ Login }/>
        <Route path="/registration" component={ Registration }/>
        <Route path="/" component={ App }/>
    </Switch>;


export default AppRouter; 