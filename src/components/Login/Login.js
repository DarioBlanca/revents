import React, { Component } from 'react';
import './Login.css';
import avatar from './avatar.jpg';
import { Link, Redirect } from 'react-router-dom';

const URL = 'http://127.0.0.1:8000/login';

export class Login extends Component {

    constructor(props){
        super(props);
        this.state = {
            redirect: false,
            username: '',
            password: ''
        }

    }

    changePassword = (event) => {
        this.setState({
            password : event.target.value
        })
    }

    changeUsername = (event) => {
        this.setState({
            username : event.target.value
        })
    }

    login = (event) => {
        event.preventDefault();
        let formData = new FormData();
        formData.append('email', this.state.username);
        formData.append('password', this.state.password);
        fetch(URL, {
            method: 'post',
            body: formData
        })
        .then(
            response => response.json() 
        ).then(
            json => {
                localStorage.setItem('token', json.token);
                this.setState({
                    redirect: true
                })
            }
        )
    };

    registrarse = (event) => {
        event.preventDefault();
        alert(localStorage.getItem('token'));
    }

    render(){
        const { redirect } = this.state;
        if (redirect){
            return <Redirect to="/" />
        }
        return (
            <div className="wrapper fadeInDown mt-3">
                <div id="formContent">
                    <div className="fadeIn first">
                        <img src={avatar} className="avatar" id="icon" alt="User Icon" />
                    </div>

                    <form>
                        <input type="text" id="login" className="fadeIn second" onChange={this.changeUsername.bind(this)} placeholder="Username"/>
                        <input type="password" id="password" className="fadeIn third" onChange={this.changePassword.bind(this)} placeholder="Password"/>
                        <input type="submit" className="fadeIn fourth" onClick={this.login.bind(this)} value="Log In"/>
                    </form>

                    <div id="formFooter">
                        <Link className="underlineHover" to="/registration">Aún no tengo cuenta</Link>
                    </div>

                </div>
            </div>
        );
    }
}

export default Login;