import React, { Component } from 'react';
import './Registration.css';
import  { Redirect } from 'react-router-dom';

const URL = 'http://127.0.0.1:5000/nuevoUsuario'

export class Registration extends Component {

    constructor(props){
        super(props);
        this.state = {
            redirect : false
        }
    }

    handleClick = (event) => {
        event.preventDefault();
        let formData = new FormData();
        formData.append('nombre', document.getElementById('nombre').value);
        formData.append('apellido', document.getElementById('apellido').value);
        formData.append('email', document.getElementById('email').value);
        formData.append('email', document.getElementById('email').value);
        formData.append('password', document.getElementById('password').value);
        
        fetch(URL, {
            method: 'post',
            body: formData
        }).then(
            response => {
                if (response.status === 200) {
                    return response.json()
                }
            }
        ).then(
            json => {
                localStorage.setItem('token', json.token);
                this.setState({
                    redirect: true
                })
            }
        )
    }

    render(){
        const { redirect } = this.state;
        if (redirect || localStorage.getItem('token')){
            return <Redirect to="/" />
        }
        return (
            <div className="wrapper fadeInDown mt-3">
                <div id="formContent">
                    <div className="fadeIn first">
                        <h3 className="title">Revents</h3>
                    </div>

                    <form>
                        <input type="text" id="nombre" className="fadeIn second" placeholder="Nombre"/>
                        <input type="text" id="apellido" className="fadeIn second" placeholder="Apellido"/>
                        <input type="text" id="email" className="fadeIn second" placeholder="Email"/>
                        <input type="text" id="password" className="fadeIn third" placeholder="Password"/>
                        <input type="submit" className="fadeIn fourth" value="Log In" onClick={this.handleClick.bind(this)}/>
                    </form>
                </div>
            </div>
        )
    }
}

export default Registration;