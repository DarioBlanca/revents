import React, { Component } from 'react'
import { ActivityCard } from '../ActivityCard/ActivityCard';
import './ActivityList.css';
import $ from 'jquery';

const URL_BASE = "http://127.0.0.1:8000";
const URL_API_ASISTIR = URL_BASE + '/nuevaAsistira';
const URL_API_BAJA = URL_BASE + '/bajaAsistira';


export class ActivityList extends Component {

    constructor(props){
        super(props);
        this.state = {
            actividades: [],
            url: URL_BASE + this.props.api
        };
        this.handleClickAsistir = this.handleClickAsistir.bind(this);
        this.updateState = this.updateState.bind(this);
    }

    componentDidMount() {
        const { api } = this.props;
        if (!localStorage.getItem(api)){
            fetch(this.state.url, {
                method: 'get',
                headers: {
                    'Authorization': localStorage.getItem('token')
                }
            }).then(
                response => response.json()
            ).then(
                json => {
                    localStorage.setItem( api, JSON.stringify(json.actividades));
                    this.setState({
                        actividades: json.actividades
                    })
                }
            );
        } else {
            if (this.state.actividades.length === 0 && localStorage.getItem(api)){  
                this.setState({
                    actividades: JSON.parse(localStorage.getItem(api))
                });
            }
        }
    }

    handleClickAsistir = (actividad, id) => {
        let elemento = '/actividades/asistire';
        let dato_final = localStorage.getItem(elemento);
        dato_final = JSON.parse(dato_final);
        let index = -1;
        for (var i = 0; i < dato_final.length; i++){
            if (JSON.stringify(dato_final[i] === JSON.stringify(actividad))){
                index = i;
            }
        }
        
        let formData = new FormData();
        formData.append('actividad_id', actividad.id);
        let URL = ( index <= -1 ? URL_API_ASISTIR : URL_API_BAJA );
        fetch(URL, {
            method: 'post',
            headers: {
                'Authorization': localStorage.getItem('token')
            },
            body: formData
        }).then(
          response => response.json()
        ).then(
            json => {
                if (index <= -1){
                    dato_final.push(actividad);
                } else {
                    dato_final.splice(index, 1);
                }

                localStorage.setItem(elemento, JSON.stringify(dato_final));
                this.props.handleClickAsistir();

                let actividades_aux = this.state.actividades;
                const foundIndex = actividades_aux.findIndex(elemento => elemento.id === id);
                actividades_aux[foundIndex].asistira = ( actividades_aux[foundIndex].asistira == "si" ? "no" : "si");
                this.setState({
                    actividades: actividades_aux
                })
            }
        )
    }

    updateState = () => {
        this.setState({
            actividades: JSON.parse(localStorage.getItem(this.props.api))
        })
    }

    componentDidCatch(){
        console.log(this.state.actividades);
    }

    render(){
        const { actividades } = this.state;
        const { ajena } = this.props;
        return (
            <div className="card-group row-eq-height">
                { actividades ? 
                    actividades.map(actividad => {
                        return <ActivityCard actividad={actividad} 
                                            key={actividad.id} 
                                            id={actividad.id}
                                            ajena={ajena}
                                            asistira={actividad.asistira} 
                                            handleClick={this.handleClickAsistir}/>
                    }) 
                    : <h4>Sin actividades</h4>  }
            </div>
        )
    }
}