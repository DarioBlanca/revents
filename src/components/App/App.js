import React, { Component } from 'react';
import './App.css';
import { Navbar } from '../Navbar/Navbar';
import { ActivityList} from '../ActivityList/ActivityList';
import { Redirect } from 'react-router-dom';

const URL = 'http://127.0.0.1:8000/activities'

export class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
    };
    this.reference = React.createRef();
  }

  componentWillMount(){
    if (localStorage.getItem('token') === null){
      this.setState({
        redirect: true
      }) 
    }
  }

  handleClickAsistir = () => {
    this.reference.current.updateState();
  }

  render() {
    const { redirect } = this.state;
    if (redirect){
      return <Redirect to="/login" />
    }

    return (
      <div>
        <Navbar/>
        <h3>Mis actividades</h3>
        <ActivityList api="/actividades/creadas" ajena="no"/>
        <h3>Otras actividades</h3>
        <ActivityList api="/actividades/otras" className="mt-3" ajena="si" handleClickAsistir={ this.handleClickAsistir }/>
        <h3>Actividades a las que asistiras</h3>
        <ActivityList api="/actividades/asistire" className="mt-3" ajena="no" ref={this.reference}/>
      </div>
    );
  }
}

export default App;
