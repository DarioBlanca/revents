import React, { Component } from 'react';
import './ActivityCard.css';
import { Link } from 'react-router-dom';

export const ActivityCard = (props) => {
  const { actividad , handleClick, ajena, asistira} = props;
    return (
      <div className="card col-sm-3 text-center m-3">
          <img className="card-img-top" src={actividad.imagen} alt="Sobre la actividad"/>
          <div className="card-body">
              <h5 className="card-title">{actividad.titulo}</h5>
              <p className="card-text">{actividad.descripcion}</p>
          </div>
          <div className="card-footer">
            <Link to="/" className="btn btn-primary">Leer más</Link>

            { ajena === 'si' && 
              <button className={ asistira === 'no' ? "btn btn-primary ml-3" : "btn btn-secondary ml-3"} 
              onClick={() => handleClick(actividad, props.id)} >{ asistira === 'no' ? "Asistir" : "Cancelar" }</button>                
            }
          </div>
      </div>
    )
};

export default ActivityCard;
