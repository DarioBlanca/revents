import React, { Component } from 'react'

export class Navbar extends Component {
  render() {
    return (
      <div>
        <div className="navbar navbar-dark bg-dark">
          <a className="navbar-brand" href="#">Revents</a>
        </div>
      </div>
    )
  }
}

export default Navbar
